
---tworzenie tabeli
CREATE TABLE pracownicy
(
  id int(11) NOT NULL AUTO_INCREMENT,
  imie varchar(20) DEFAULT NULL,
  nazwisko varchar(20) DEFAULT NULL,
  dzial int(11) DEFAULT NULL,
  zarobki int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) CHARACTER SET=utf8;
--- default - wartość domyślna

---wyświetlenie instniejących tabel
SHOW TABLES;

---struktóra tabeli
DESC moja_tabela;

---kod tabeli
SHOW CREATE TABLE moja_tabela;

---usuwanie tabeli
DROP TABLE moja_tabela;

---dodawanie wartości do tabeli
INSERT INTO moje_kontakty
(
	nazwisko, imie, email, data_urodzenia, zawod, lokalizacja, stan, zainteresowania, szuka
)
VALUES
(
	'Kowalski', 'Jan', 'cokolwiek@gmail.com', '1945-01-30',
	'planista', 'Warszawa', 'M', 'Aikodo', 'wrażeń, spokoju'
);

---dodawanie tylko niektórych wartości do tabeli
INSERT INTO moje_kontakty
( nazwisko, imie, email)
VALUES
( 'Burczymucha', 'Stefan', 'cosik@interia.pl');

---uaktualnienie wartości tabeli
UPDATE moje_kontakty
SET telefon = '555-33'
WHERE id = 2;
