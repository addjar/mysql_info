---wyświetlenie wszystkich wartości w tabeli
SELECT * FROM moja_tabela;

---wyświetlenie poszczególnych kolumn
SELECT pierwsza_kolumna, druga_kolumna FROM moja_tabela;

---WHERE - gdzie, precyzowanie zapytania
SELECT * FROM WHERE kolumna = 'jakas_wartosc'
